% To plot Stiffness Results
x = [ 14.5 16 18.25 22.5];

y_4 = 10^-4*[ 3131 3359 3601 3892];
y_air =10^-4*[ 2663 2800 3017 3355];
y_2 = 10^-4*[ 2800 2982.2 3251 3599];
y_1 = 10^-4*[ 2721 2911 3146 3475];
y_8 = 10^-4*[ 3449 3651 3861 4121];
y_p = 10^-4*[ 3647 3825 4011 4291];


F_4 = (0.03975^-1)*10^-4*[ 3131 3359 3601 3892];
F_air = (0.03975^-1)*10^-4*[ 2663 2800 3017 3355];
F_2 = (0.03975^-1)*10^-4*[ 2800 2982.2 3251 3599];
F_1 =(0.03975^-1)*10^-4*[ 2721 2911 3146 3475];
F_8 = (0.03975^-1)*10^-4*[ 3449 3651 3861 4121];
F_p = (0.03975^-1)*10^-4*[ 3647 3825 4011 4291];

grid on
hold on

plot(x,y_air,'sk--','MarkerFaceColor','y')
plot(x,y_1,'dk--','MarkerFaceColor','r')
plot(x,y_2,'>k--','MarkerFaceColor','b')
plot(x,y_4,'^k--','MarkerFaceColor','k')
plot(x,y_8,'<k--','MarkerFaceColor','g')
plot(x,y_p,'pk--','MarkerFaceColor','m')
ylabel('Measured Stain %')
 xlabel('Observed Tip Angle \theta (Degrees)')



% 
% ylabel ('Force N')
% plot(x,F_air,'sk--','MarkerFaceColor','y')
% plot(x,F_1,'dk--','MarkerFaceColor','r')
% plot(x,F_2,'>k--','MarkerFaceColor','b')
% plot(x,F_4,'^k--','MarkerFaceColor','k')
% plot(x,F_8,'<k--','MarkerFaceColor','g')
% plot(x,F_p,'pk--','MarkerFaceColor','m')

legend('Air','1:1 - 6.543 kPa', '2:1 - 13.54 kPa','4:1 - 24.573 kPa', '8:1 - 57.293 kPa','Plastic - 67.11 kPa','Location','nw');