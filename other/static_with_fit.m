figure
grid on
hold on
x=theta;
plot(x,y_p,'pk--','MarkerFaceColor','m')
plot(x,y_8,'<k--','MarkerFaceColor','g')
plot(x,y_4,'^k--','MarkerFaceColor','k')
plot(x,y_2,'>k--','MarkerFaceColor','b')
plot(x,y_1,'dk--','MarkerFaceColor','r')
plot(x,y_air,'sk--','MarkerFaceColor','y')



ylabel('Strain [%]')
xlabel('Observed Tip Flexion \theta [Degrees]');
legend('Plastic - 67.11 kPa', '8:1 - 57.293 kPa','4:1 - 24.573 kPa', '2:1 - 13.54 kPa','1:1 - 6.543 kPa','Air','Location','nw');
