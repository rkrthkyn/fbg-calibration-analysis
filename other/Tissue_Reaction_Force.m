% To plot Stiffness Results
x = [14.5 16 18.25 22.5];

y_4 = 10^-4*[3131 3369 3701 4192];
y_air =10^-4*[ 2663 2800 3017 3355];
y_2 = 10^-4*[ 2800 2982 3301 3844];
y_1 = 10^-4*[ 2721 2891 3206 3775];
y_8 = 10^-4*[ 3449 3651 3961 4461];
y_p = 10^-4*[ 3647 3825 4111 4551];
k = (0.03975^-1);

F_4 = k*y_4;
F_air = k*y_air;
F_2 = k*y_2;
F_1 = k*y_1;
F_8 = k*y_8;
F_p = k*y_p;
F_bg_3 = k*y_bg_3;

% Reaction Components

R_4 = F_4 - F_air;
R_2 = F_2 - F_air;
R_1 = F_1 - F_air;
R_8 = F_8 - F_air;
R_p = F_p - F_air;
R_bg_3 = F_bg_3-F_air;

figure
grid on
hold on
k1=0.425;
plot(x,y_p+k1,'pk--','MarkerFaceColor','m')
plot(x,y_8+k1,'<k--','MarkerFaceColor','g')
plot(x,y_4+k1,'^k--','MarkerFaceColor','k')
plot(x,y_2+k1,'>k--','MarkerFaceColor','b')
plot(x,y_1+k1,'dk--','MarkerFaceColor','r')
plot(x,y_air+k1,'sk--','MarkerFaceColor','y')



ylabel('Strain [%]')
xlabel('Observed Tip Flexion \theta [Degrees]');
legend('Plastic - 67.11 kPa', '8:1 - 57.293 kPa','4:1 - 24.573 kPa', '2:1 - 13.54 kPa','1:1 - 6.543 kPa','Air','Location','nw');

figure



hold on

xlabel('Observed tip flexion (\theta^\circ)')
ylabel ('Tissue reaction force R_t(N)')
c = 0.127;
% plot(x,F_air,'sk--','MarkerFaceColor','y')
plot(x,c*R_p,'pk--','MarkerFaceColor','m')
plot(x,c*R_8,'<k--','MarkerFaceColor','g')
plot(x,c*R_4,'^k--','MarkerFaceColor','k')
plot(x,c*R_2,'>k--','MarkerFaceColor','b')
plot(x,c*R_1,'dk--','MarkerFaceColor','r')


legend('Plastic - 67.11 kPa','8:1 - 57.293 kPa','4:1 - 24.573 kPa', '2:1 - 13.54 kPa','1:1 - 6.543 kPa', 'Location','nw');


% figure
% plot(x,y_p,'pk--','MarkerFaceColor','m')
% plot(x,y_8,'<k--','MarkerFaceColor','g')
% plot(x,y_4,'^k--','MarkerFaceColor','k')
% plot(x,y_2,'>k--','MarkerFaceColor','b')
% plot(x,y_1,'dk--','MarkerFaceColor','r')
