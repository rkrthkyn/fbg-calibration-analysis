
% This plots combined data points form 250 g and 500 g load test to provide
% a load to strain linearity check!

% strain = 11.57*load+44.5 from Reference plot and tendon transmission
% model
close all;

format long
a = dlmread('A_1.txt');   % Read Femto-sense data
b = dlmread('P_2.txt');
c = dlmread('P_3.txt');

% A

lambda = a(:,6).*10^12;             % Extract Wavelength Column

if lambda(1,1) < lambda(2,1)
  lambda1= lambda(1:2:end);   % Isolate Peak 1 Wavelengths
lambda2= lambda(2:2:end);   % Isolate Peak 2 Wavelengths  
else
    lambda2= lambda(1:2:end);   % Isolate Peak 1 Wavelengths
lambda1= lambda(2:2:end);   % Isolate Peak 2 Wavelengths
end

Sweep_count          = a(:,2);              % Extract Sweep Number
sweepcount1          = Sweep_count(1:2:end); 
sweepcount2          = Sweep_count(2:2:end);

  
lambda1_0 = mean(lambda1(1:5000,:));       % Reference wavelength 1
lambda2_0 = mean(lambda2(1:5000,:));       % Reference wavelength 2


% B

lambda_b = b(:,6).*10^12;             % Extract Wavelength Column

if lambda_b(1,1) < lambda_b(2,1)
  lambda_b1= lambda_b(1:2:end);   % Isolate Peak 1 Wavelengths
lambda_b2= lambda_b(2:2:end);   % Isolate Peak 2 Wavelengths  
else
    lambda_b2= lambda_b(1:2:end);   % Isolate Peak 1 Wavelengths
lambda_b1= lambda_b(2:2:end);   % Isolate Peak 2 Wavelengths
end

Sweep_count_b          = b(:,2);              % Extract Sweep Number
sweepcount1_b          = Sweep_count_b(1:2:end); 
sweepcount2_b        = Sweep_count_b(2:2:end);

  
lambda_b1_0 = mean(lambda_b1(1:5000,:));       % Reference wavelength 1
lambda_b2_0 = mean(lambda_b2(1:5000,:));       % Reference wavelength 2


% C

lambda_c = c(:,6).*10^12;             % Extract Wavelength Column

if lambda_c(1,1) < lambda_c(2,1)
  lambda_c1= lambda_c(1:2:end);   % Isolate Peak 1 Wavelengths
lambda_c2= lambda_c(2:2:end);   % Isolate Peak 2 Wavelengths  
else
    lambda_c2= lambda_c(1:2:end);   % Isolate Peak 1 Wavelengths
lambda_c1= lambda_c(2:2:end);   % Isolate Peak 2 Wavelengths
end

Sweep_count_c          = c(:,2);              % Extract Sweep Number
sweepcount1_c          = Sweep_count_c(1:2:end); 
sweepcount2_c          = Sweep_count_c(2:2:end);

  
lambda_c1_0 = mean(lambda_c1(1:2000,:));       % Reference wavelength 1
lambda_c2_0 = mean(lambda_c2(1:2000,:));       % Reference wavelength 2

% Sensitivity Parameters  

a = 13.72;                 
b = 1.176;
c = 12.71;
d = 1.197;
D = a*d - b*c;

% Wavelength Change Values

% A
n = length(lambda1);
delta_l1 = zeros(n,1);
delta_l2 = zeros(n,1);

for i = 1:n
    delta_l1(i) = lambda1(i) - lambda1_0;
    delta_l2(i) = lambda2(i) - lambda2_0;
end

delta_t = (d.*delta_l1 - b.*delta_l2)/D;
delta_strain = (a.*delta_l2 - c.*delta_l1)/D;

% ===============================

k1 = length(sweepcount1);
ref1 = sweepcount1(1);

for q = 1:k1
    sweepcount1(q) = sweepcount1(q)-ref1;
end

k2 = length(sweepcount1);
ref2 = sweepcount2(1);

for q = 1:k2
    sweepcount2(q) = sweepcount2(q)-ref2;
end

% Time-Stamp for each peak
time1 = sweepcount1./1000;
time2 = sweepcount2./1000;

time_avg = 0.5*(time1+time2);


% B

n = length(lambda_b1);
delta_bl1 = zeros(n,1);
delta_bl2 = zeros(n,1);

for i = 1:n
    delta_bl1(i) = lambda_b1(i) - lambda_b1_0;
    delta_bl2(i) = lambda_b2(i) - lambda_b2_0;
end

delta_bt = (d.*delta_bl1 - b.*delta_bl2)/D;
delta_bstrain = (a.*delta_bl2 - c.*delta_bl1)/D;

% ===============================

k1_b = length(sweepcount1_b);
ref1_b = sweepcount1_b(1);

for q = 1:k1_b
    sweepcount1_b(q) = sweepcount1_b(q)-ref1_b;
end

k2_b = length(sweepcount1_b);
ref2_b = sweepcount2_b(1);

for q = 1:k2_b
    sweepcount2_b(q) = sweepcount2_b(q)-ref2_b;
end

% Time-Stamp for each peak
time1 = sweepcount1_b./1000;
time2 = sweepcount2_b./1000;

time_avg_b = 0.5*(time1+time2);



% C

n = length(lambda_c1);
delta_cl1 = zeros(n,1);
delta_cl2 = zeros(n,1);

for i = 1:n
    delta_cl1(i) = lambda_c1(i) - lambda_c1_0;
    delta_cl2(i) = lambda_c2(i) - lambda_c2_0;
end

delta_ct = (d.*delta_cl1 - b.*delta_cl2)/D;
delta_cstrain = (a.*delta_cl2 - c.*delta_cl1)/D;

% ===============================

k1_c = length(sweepcount1_c);
ref1_c = sweepcount1_c(1);

for q = 1:k1_c
    sweepcount1_c(q) = sweepcount1_c(q)-ref1_c;
end

k2_c = length(sweepcount1_c);
ref2_c = sweepcount2_c(1);

for q = 1:k2_c
    sweepcount2_c(q) = sweepcount2_c(q)-ref2_c;
end

% Time-Stamp for each peak
time1 = sweepcount1_c./1000;
time2 = sweepcount2_c./1000;

time_avg_c = 0.5*(time1+time2);


hold on;
% plot(time_avg,delta_strain,'r')
str1 = [0 308.2 589.8 867.5 1146 1391];
ang1 = [0 4.17157 8.68137+2.05882 12.951+2.39216 18.3627+1.81373 23.6569+2.36765];
% figure


ang2=[0 2.617 7.617+1.475 13.1127+1.5098 18.24+1.48529 1.51471+23.7108];
str2 = [0 308.7 706.7 1054 1319 1598];
plot (ang1, str1,'sb');
plot( ang2,str2,'sr');
plot(fittedmodel,'--r');
plot(fittedmodel1,'--b');
legend ('Phantom Test Data','In-air Test Data','Location','northwest');
xlabel('Angular Position of Actuator Shaft (^\circ)');
ylabel('\muStrain');












% grid on
% 
% hold on
% 
% plot(time_avg,delta_t,'r')
% xlabel('time (s)')
% ylabel('Temperature change (deg C)')


% 
% 
% plot(time_avg_b,delta_bstrain,'g')
% grid on
% xlabel('time (s)')
% 
% ylabel('Strain')


% Post-Processing

% A = [0 63 102 131 161 190 225 249 274 310];
% B = [0 103 169 242 307 371 430 476 543 586];
% C = [0 33 71 104 138 160 193 220 242 263];
% 
% % A
% X = zeros(1,length(A));
% 
% for i = 1:length(A)
%     X(i) = find(time_avg == A(i));
% end
% 
% for i = 1:length(A)
%     S(i) = delta_strain(X(i));
% end
% 
% 
% for i = 1:length(A)
%     T(i) = delta_t(X(i));
% end
% 
% % B
% 
% Y = zeros(1,length(B));
% 
% for i = 1:length(B)
%     Y(i) = find(time_avg_b == B(i));
% end
% 
% for i = 1:length(B)
%     Sb(i) = delta_bstrain(Y(i));
% end
% 
% 
% 
% for i = 1:length(B)
%     Tb(i) = delta_bt(X(i));
% end
% 
% % C
% 
% Z = zeros(1,length(C));
% 
% for i = 1:length(C)
%     Z(i) = find(time_avg_c == C(i));
% end
% 
% for i = 1:length(C)
%     Sc(i) = delta_cstrain(Z(i));
% end
% 
% 
% 
% for i = 1:length(C)
%     Tc(i) = delta_ct(Z(i));
% end
% 
% Z = sort(Z);
% Sc= sort(Sc);
% Tc= sort(Tc);
% 
% L = [0 50 70 90 110 130 150 170 190 200];
% Lb = L;
% Lc =[0 50 70 90 110 130 150 170 180 200];
% 
% 
% 

% ==============================
% figure
% hold on
% 
% plot(L,S,'sk','MarkerFaceColor','r');
% 
% plot(Lc,Sc,'sk','MarkerFaceColor','b');
% 
% plot(L,Sb,'sk','MarkerFaceColor','g');
% grid on
% x1 = xlabel ('Load (g)');
% y1 = ylabel ('\muStrain');
% set(x1,'FontSize',14);
% set(y1,'FontSize',14);
% t= title('Tendon Transmission Decay with Curvature');
% set(t,'FontSize',14);
% set(gca,'YTick',[0:0.25e3:2.5e3]);
% 
% set(gca,'XTick',[0:25:250]);
% 
% 
% yyaxis right
% 
% plot(L,T+25,'.--b'); 
% y2 = ylabel('Temperature (^\circC)');
% set(y2,'FontSize',14);
% ylim([0 100]);

% leg = legend('120 Degrees','90 Degrees','45 Degrees','Location','northwest');
% set(leg,'FontSize',14);
%  v = get(leg,'title');
% set(v,'string','Curvature Angles');
% xlim([0 250]);
% 
% 
% 
% % A
% lout_A = (S-44.5)./11.57;
% ratio = rdivide(lout_A,L);
% ln_ratio = log(ratio);
% mu = -1* ln_ratio.*2.*pi/3;
% 
% %B 
% lout_b = (Sb-44.5)./11.57;
% ratio_b = rdivide(lout_b,L);
% ln_ratio_b = log(ratio_b);
% mu_b = -1* ln_ratio_b.*pi/4;
% 
% 
% %C
% lout_c = (Sc-44.5)./11.57;
% ratio_c = rdivide(lout_c,L);
% ln_ratio_c = log(ratio_c);
% mu_c = -1* ln_ratio_c.*pi/2;
% 
% figure
% hold on;
% plot(L(1,2:end),mu(1,2:end),'s');
% plot(Lb(1,2:end),mu_b(1,2:end),'s');
% plot(Lc(1,2:end),mu_c(1,2:end),'s');