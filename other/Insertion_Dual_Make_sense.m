%% Insertion Sort Data
[Input] = xlsread('d_Phantom_Plots.xlsx');
k=0.1256^-1;
%V1
xdata1 = Input(2:end,1);
ydata1 = Input(2:end,2);
fdata1 = k*(ydata1+0.06455+0.001279);

%V3

ydata3 = Input(3:end,8);
xdata3 = Input(3:end,7);
fdata3 = k*(ydata3+0.02589+0.009482);


%V2

ydata2 = Input(2:end,5);
xdata2 = Input(2:end,4);
fdata2 = k*(ydata2+0.1607);




% fc2 = 50;
% fc1 = 30;
% fc3 = 499;
% fs1 = 1000;
% 
% 
% figure
% hold on
% [b1,a1] = butter(6,fc1/(fs1/2));
% freqz(b1,a1)
% [b2,a2] = butter(6,fc2/(fs1/2));
% freqz(b2,a2);
% [b3,a3] = butter(6,fc3/(fs1/2));
% freqz(b3,a3);
% 
% dataOut = filter(b1,a1,ydata2);
% dataOut = filter(b2,a2,dataOut);
% dataOut = filter(b3,a3,dataOut);


%% plots


figure
hold on

plot(xdata1,ydata1+0.06455+0.001279,'r')
plot(xdata2,ydata2+0.1607,'k')
plot(xdata3,ydata3+0.02589+0.009482,'b')

yyaxis right

plot(xdata1,fdata1,'r')
plot(xdata2,fdata2,'k')
plot(xdata3,fdata3,'b')



grid on
box on
