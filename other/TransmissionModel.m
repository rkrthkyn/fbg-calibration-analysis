
% This plots combined data points form 250 g and 500 g load test to provide
% a load to strain linearity check!


close all;
clear all;
format long
% Read Femto-sense data

a = dlmread('Initial1.4.txt');   
 % Extract Wavelength Column 120

lambda = a(:,6).*10^12;            

if lambda(1,1) < lambda(2,1)
  lambda1= lambda(1:2:end);   % Isolate Peak 1 Wavelengths
lambda2= lambda(2:2:end);   % Isolate Peak 2 Wavelengths  
else
    lambda2= lambda(1:2:end);   % Isolate Peak 1 Wavelengths
lambda1= lambda(2:2:end);   % Isolate Peak 2 Wavelengths
end

Sweep_count          = a(:,2);              % Extract Sweep Number
sweepcount1          = Sweep_count(1:2:end); 
sweepcount2          = Sweep_count(2:2:end);

  
lambda1_0 = mean(lambda1(1:100,:));       % Reference wavelength 1
lambda2_0 = mean(lambda2(1:100,:));       % Reference wavelength 2

% Sensitivity Parameters  

a = 13.72;                 
b = 1.176;
c = 12.71;
d = 1.197;
D = a*d - b*c;

% Wavelength Change Values

n = length(lambda1);
delta_l1 = zeros(n,1);
delta_l2 = zeros(n,1);

for i = 1:n
    delta_l1(i) = lambda1(i) - lambda1_0;
    delta_l2(i) = lambda2(i) - lambda2_0;
end

delta_t = (d.*delta_l1 - b.*delta_l2)/D;
delta_strain = (a.*delta_l2 - c.*delta_l1)/D;

% ===============================

k1 = length(sweepcount1);
ref1 = sweepcount1(1);

for q = 1:k1
    sweepcount1(q) = sweepcount1(q)-ref1;
end

k2 = length(sweepcount1);
ref2 = sweepcount2(1);

for q = 1:k2
    sweepcount2(q) = sweepcount2(q)-ref2;
end

% Time-Stamp for each peak
time1 = sweepcount1./1000;
time2 = sweepcount2./1000;

time_avg = 0.5*(time1+time2);

hold on

%  str = [ 0 531.8 1195 1486 1785 2000];
%  angle_th = [0 5 10 15 20 25];
%  plot (angle_th,str,'*-');
%  angle_act = [ 0 2.73 10.348 17.136 24.274 31.048];
%  plot (angle_act,str);
%  angle_act_2 = [ 0 2.73 5.928 8.53 12.208 14.972];
% plot (angle_act_2,str);
 % grid on

% 
% hold on
% 
% plot(time_avg,delta_t,'r')
% xlabel('time (s)')
% ylabel('Temperature change (deg C)')
% L = [0 50 70 90 110 130 150 170 190 200];
% Y1= [0 562.7 793.1 1023 1253 1491 1715 1949 2182 2295]
% L2= [0 50 70 90 110 130 150 170 200];
% Y2= [-0.4754 533.7 719.6 921.2 1149 1379 1592 1828 2178];
% YY2 = [0 578.8 711.4 928.6 1139 1388 1652 1800 2036 2485];
% Y3= [-0.4778 491.7 696.2 901.6 1118 1351 1577 1796 2028 2147];

hold on;
plot(time_avg,delta_strain,'r')
% str1 = [0 308.2 589.8 867.5 1146 1391];
% ang1 = [0 4.17157 8.68137+2.05882 12.951+2.39216 18.3627+1.81373 23.6569+2.36765];
% figure


% ang2=[0 2.617 7.617+1.475 13.1127+1.5098 18.24+1.48529 1.51471+23.7108];
% str2 = [0 308.7 726.7 1119 1319 1564];
% plot (ang1, str1,'sb--');
% plot( ang2,str2,'sr--');
% legend ('Phantom Test Data','In-air Test Data','Location','northwest');
% xlabel('Angular Position of  (^/circC))')

% grid on
% figure
% plot(L,Y1,'g')
% hold on
% plot(L2,Y2,'r')
% hold on
% %plot(L,YY2,'r')
% hold on
% plot(L,Y3,'b')
% grid on
% xlabel('time (s)')
% 
% ylabel('Strain')


% Post-Processing

% A = [0 63 102 131 161 190 225 249 274 310];
% X = zeros(1,length(A));
% 
% for i = 1:length(A)
%     X(i) = find(time_avg == A(i));
% end
% 
% for i = 1:length(A)
%     S(i) = delta_strain(X(i));
% end
% figure 
% 
% for i = 1:length(A)
%     T(i) = delta_t(X(i));
% end
% 
% 
% L = [0 50 70 90 110 130 150 170 190 200];
% 
% 
% 
% 
% % ==============================
% 
% hold on
% 
% plot(L,S,'*--r')
% grid on
% x1 = xlabel ('Load (g)');
% y1 = ylabel ('\muStrain');
% set(x1,'FontSize',14);
% set(y1,'FontSize',14);
% t= title('Transmission Test 120 Degree Curvature Angle');
% set(t,'FontSize',14);
% 
% yyaxis right
% 
% plot(L,T+25,'.--b'); 
% y2 = ylabel('Temperature (^\circC)');
% set(y2,'FontSize',14);
% ylim([0 100]);
% 
% leg = legend('Strain Change','Temperature','Location','northwest');
% set(leg,'FontSize',14);
% 
% xlim([0 200]);
