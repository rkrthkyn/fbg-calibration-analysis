%% ---------------------------------------------
% Let's compare the behavior of flexure and hinge
% Rohith Karthikeyan 07/24/2018 Tuesday
% ---------------------------------------------

%% Comments
% 1. For a given deltaL the flexure produces a greater deflection than the
% hinge, however, a single hinge of comparable dimensions as the flexure
% can produce a larger deflection. 
% 2. A single slit can produce a max deflection of ~11 degrees.
% 3. For equivalent deflections, the swept volume of the hinge is lower
% than that of the flexure.

clc
clear all
close all

%% Definitions and constants
slit.depth = 0.55;
slit.width = 0.1275;
slit.separation = 0.49;
slit.count = 1;
slit.r_i = 0.5*1.016;
slit.r_o = 0.5*1.27;

hinge.H = 0.43;
hinge.r = 0.5*1.2446;
hinge.L = 1.0795;
hinge.neutralTheta = asind(hinge.H/hinge.L);

%% Let's get some detail
deltaL = 0:0.02:0.43;
for i =1:length(deltaL)
[k,s,thetaMaxFlexure]= getCurvatureandArcLength(deltaL(i),slit);
thetaFlexure(i) = k*s*180/pi;
thetaHinge(i) = getHingeDeflection(hinge,deltaL(i));
theMax(i) = thetaMaxFlexure;
end

hold on
plot(thetaFlexure,deltaL,'s');
plot(thetaHinge,deltaL,'^');
box on 
grid on
ylabel('\Delta L')
xlabel('\Delta \theta')
legend('Flexure', 'Hinge','Location','northwest');

% figure
% i=1;
%   yB = getNeutralAxis(slit.r_i,slit.r_o,slit);
% for H = 0:0.05:1.024
%   thetaMaxFlexure(i) = slit.count*(H/(slit.r_o+yB))*180/pi;
% i=i+1;
% end
% xH = 0:0.05:1.024;


figure

thMax = 7.5708;
i =1;
for ns = 1:20
thCurr(i) = ns*thMax;
span(i) = (4*(ns)-1)*slit.width;
i =i+1;
end
plot(span, thCurr)
xlabel('Span')
ylabel('\theta_{max} (deg)')
grid on

%% function definitions
function [k,s,thetaMaxFlexure] = getCurvatureandArcLength(deltaL, slit)
yBar = getNeutralAxis(slit.r_i, slit.r_o,slit);
k = deltaL/(slit.width.*(slit.r_i+yBar)-deltaL.*yBar);
s = slit.width/(1+yBar*k);
thetaMaxFlexure = slit.count*(slit.width/(slit.r_o+yBar))*180/pi;
end

function [yBar] = getNeutralAxis(r_i,r_o,slit)
phi_i = 2*acos((slit.depth-r_o)/r_i);
y_i = 4.*r_i*sin(0.5*phi_i)^3/(3*(phi_i-sin(phi_i)));
A_i = (r_i^2)*(phi_i -sin(phi_i))/2;

phi_o = 2*acos((slit.depth-r_o)/r_o);
y_o = 4.*r_o*sin(0.5*phi_o)^3/(3*(phi_o-sin(phi_o)));
A_o = (r_o^2)*(phi_o -sin(phi_o))/2;

yBar = (y_o*A_o - y_i*A_i)/(A_o-A_i);
end

function[theta] = getHingeDeflection(hinge, deltaL)
deflectedAngle = asind((hinge.H-deltaL)/hinge.L);
theta = abs(deflectedAngle-hinge.neutralTheta);
end


