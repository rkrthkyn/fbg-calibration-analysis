% Read File for Data
A = xlsread('PhantomA_10_70.xlsx');

% plot(A(1:end,1),A(1:end,2))

X = A(1:end,1);
Y = A(1:end,2);
% Isolate Interface Regions for FFT Analysis
figure
plot(X,Y)
% First Interface
X_R1 = A(8891:10656,1);
Y_R1 = A(8891:10656,2);

% Second Interface
X_R2 = A(24871:27291,1);
Y_R2 = A(24871:27291,2);

Ref1 = X_R1(1); 
for i= 1:length(X_R1)
X_R1(i) = X_R1(i)-Ref1;
end

Ref2 = X_R2(1); 
for i= 1:length(X_R2)
X_R2(i) = X_R2(i)-Ref2;
end

figure
plot(X_R2,Y_R2);
xlabel('Time s');
ylabel('Percentage Strain %');
figure
plot(X_R1,Y_R1);
xlabel('Time s');
ylabel('Percentage Strain %');

Fs = 500;   % Sampling Frequency
Time1 = 4.84;
Time2 = 3.53;
L1 = Time1*Fs;
L2 = Time2*Fs;

Y_R2 = Y_R2-mean(Y_R2);
%Y_R2 = detrend(Y_R2);

Y_R1 = Y_R1-mean(Y_R1);
%Y_R1 = detrend(Y_R1);

% For Interface 1

Y_R1_mod = fft(Y_R1);
P2_1 = abs(Y_R1_mod/L1);
P1_1 = P2_1(1:L1/2+1);
P1_1(2:end-1) = 2*P1_1(2:end-1);

f_1 = Fs*(0:(L1/2))/L1;
figure
plot(f_1,P1_1)
title('Single-Sided Amplitude Spectrum of X(t)')
xlabel('f (Hz)')
ylabel('|P1(f)|')

% For Interface 2

Y_R2_mod = fft(Y_R2);
P2_2 = abs(Y_R2_mod/L2);
P1_2 = P2_2(1:L2/2+1);
P1_2(2:end-1) = 2*P1_2(2:end-1);

f_2 = Fs*(0:(L2/2))/L2;
figure
plot(f_2,P1_2)
title('Single-Sided Amplitude Spectrum of X(t)')
xlabel('f (Hz)')
ylabel('|P1(f)|')





