
% This plots combined data points form 250 g and 500 g load test to provide
% a load to strain linearity check!



format long
a = dlmread('PM_FBg_TEST_4_250g.txt');   % Read Femto-sense data


lambda = a(:,6).*10^12;            % Extract Wavelength Column

if lambda(1,1) < lambda(2,1)
  lambda1= lambda(1:2:end);   % Isolate Peak 1 Wavelengths
lambda2= lambda(2:2:end);   % Isolate Peak 2 Wavelengths  
else
    lambda2= lambda(1:2:end);   % Isolate Peak 1 Wavelengths
lambda1= lambda(2:2:end);   % Isolate Peak 2 Wavelengths
end

Sweep_count          = a(:,2);              % Extract Sweep Number
sweepcount1          = Sweep_count(1:2:end); 
sweepcount2          = Sweep_count(2:2:end);

  
lambda1_0 = mean(lambda1(1:10000,:));       % Reference wavelength 1
lambda2_0 = mean(lambda2(1:10000,:));       % Reference wavelength 2

% Sensitivity Parameters  

a = 13.72;                 
b = 1.176;
c = 12.71;
d = 1.197;
D = a*d - b*c;

% Wavelength Change Values

n = length(lambda1);
delta_l1 = zeros(n,1);
delta_l2 = zeros(n,1);

for i = 1:n
    delta_l1(i) = lambda1(i) - lambda1_0;
    delta_l2(i) = lambda2(i) - lambda2_0;
end

delta_t = (d.*delta_l1 - b.*delta_l2)/D;
delta_strain = (a.*delta_l2 - c.*delta_l1)/D;

% ===============================

k1 = length(sweepcount1);
ref1 = sweepcount1(1);

for q = 1:k1
    sweepcount1(q) = sweepcount1(q)-ref1;
end

k2 = length(sweepcount1);
ref2 = sweepcount2(1);

for q = 1:k2
    sweepcount2(q) = sweepcount2(q)-ref2;
end

% Time-Stamp for each peak
time1 = sweepcount1./1000;
time2 = sweepcount2./1000;

time_avg = 0.5*(time1+time2);

% grid on
% 
% hold on
% 
% plot(time_avg,delta_t,'r')
% xlabel('time (s)')
% ylabel('Temperature change (deg C)')




% plot(time_avg,delta_strain,'g')
% grid on
% xlabel('time (s)')
% 
% ylabel('Strain')


% Post-Processing

A= [12 35 50 71 96 123 153 182 212 246 281 305 325];
X = zeros(1,length(A));
for i = 1:13
    X(i) = find(time_avg == A(i));
end

for i = 1:13
    S(i) = delta_strain(X(i));
end
figure 

for i = 1:13
    T(i) = delta_t(X(i));
end
T(T>4) = 2.2;
% other - data from 500 g file

other_t = [-0.018745503309717   0.571470831365261   1.360654382793786   1.413019379873898   0.972353685520527    1.467548876375794   1.844720603826740];
aug_T = horzcat(T,other_t);

other_strain = 10^3*[  0.586030653568127   2.936965439187504   4.093787648094290   5.217235583672554   5.500322090752865  5.730209240397223   5.852972690141595]; 
aug_S = horzcat( S,other_strain);

L = [0 20 40 60 80 100 120 140 160 180 190 195 200 50 250 350 450 470 490 500];

aug_S=sort(aug_S);

L= sort(L);
aug_T = aug_T+25; % Room Temperature Increment
% ==============================

hold on

plot(L,aug_S,'sk','MarkerFaceColor','r')
plot(fittedmodel,'r--');
grid on
x1 = xlabel ('Load (g)');
y1 = ylabel ('\muStrain');
set(x1,'FontSize',14);
set(y1,'FontSize',14);
t= title('Strain and Temperature Behavior during Loading');
set(t,'FontSize',14);

yyaxis right

plot(L,aug_T,'sk','MarkerFaceColor','b'); 
plot(fittedmodel1,'b--');
y2 = ylabel('Temperature (^\circC)');
set(y2,'FontSize',14);
ylim([0 100]);

leg = legend('Strain Change','Fit Data: Strain','Temperature','Fit Data: Temperature','Location','northwest');
set(leg,'FontSize',14);


