%---------------------------
% Author: Rohith Karthikeyan
% Last edit: 10/09/2018
%---------------------------
close all;
clear all;
format long
%% Read Femto-sense data
a = dlmread('FT_T1.txt');   
% Extract Wavelength 
lambda = a(:,6).*10^12;            
if lambda(1,1) < lambda(2,1)
  lambda1= lambda(1:2:end); % Isolate Peak 1 Wavelengths
lambda2= lambda(2:2:end);   % Isolate Peak 2 Wavelengths  
else
    lambda2= lambda(1:2:end); % Isolate Peak 1 Wavelengths
lambda1= lambda(2:2:end);   % Isolate Peak 2 Wavelengths
end


Sweep_count          = a(:,2);              % Extract Sweep Number
sweepcount1          = Sweep_count(1:2:end); 
sweepcount2          = Sweep_count(2:2:end);

% These values to change depending on fiber in use  
% lambda1_0 = 1.534105501784950e+06;       % Reference wavelength 1
% lambda2_0 = 1.534930278066740e+06;       % Reference wavelength 2

lambda1_0= 1543572.68687700;
lambda2_0= 1544391.46266000;

% alternatively:
% lambda1_0 = mean(lambda1(1:100,:));       % Reference wavelength 1
% lambda2_0 = mean(lambda2(1:100,:));       % Reference wavelength 2


%% Sensitivity Parameters  

a = 13.72;                 
b = 1.176;
c = 12.71;
d = 1.197;
D = a*d - b*c;

% What to do now?
% Wavelength Change Values

n = length(lambda1);
delta_l1 = zeros(n,1);
delta_l2 = zeros(n,1);

% Isolate Lambdas of importance

for i = 1:n
    delta_l1(i) = lambda1(i) - lambda1_0;
    delta_l2(i) = lambda2(i) - lambda2_0;
end

delta_t = (d.*delta_l1 - b.*delta_l2)/D;
delta_strain = (a.*delta_l2 - c.*delta_l1)/D;

% ===============================

k1 = length(sweepcount1);
ref1 = sweepcount1(1);

for q = 1:k1
    sweepcount1(q) = sweepcount1(q)-ref1;
end

k2 = length(sweepcount1);
ref2 = sweepcount2(1);

for q = 1:k2
    sweepcount2(q) = sweepcount2(q)-ref2;
end

% Time-Stamp for each peak
time1 = sweepcount1./1000;
time2 = sweepcount2./1000;
time_avg = 0.5*(time1+time2);
hold on
T1 = (delta_strain+abs(delta_strain(1)))*10^-4;
T2 = (delta_strain-abs(delta_strain(1)))*10^-4;
X_R1 = time_avg;
Y_R1 = T1;
Y_R2 = T2;
if(delta_strain(1)<0)
   plot(time_avg,T1,'r');
else
 plot(time_avg,T2);
end
box on
grid on
xlabel('Time (s)');
ylabel('Measured Strain (%)');
  
  
      
   
   
   
   
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%BELOW: SOME STUFF FROM BEFORE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   % what the fudge was I trying to do here?

% negval = find(T1<-0.0035);   % Find indeces of values that meet condition
% zpt = negval(1,1);   % Position of first negative value
% 
% T1_trim = T1(1:zpt,1);
% max_val = max(T1_trim);
% 
% max_thresh = max_val - 0.002;
% T1_peaks = T1 > max_thresh;
% 
% T1_peak_vals = T1(T1_peaks);
% avg_val = mean(T1_peak_vals);
% disp(avg_val);